#!/usr/bin/env python
import os
import sys

from utils.env_variables import load_local_variables


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dummy.settings')
    # Load environment variables if not found in environment already
    # This should ideally trigger only on a developer's local install.
    # Production, staging and testing servers will already have
    # the env variables set up so this bit is skipped.
    if os.getenv('DEBUG') is None:
        load_local_variables()
    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
