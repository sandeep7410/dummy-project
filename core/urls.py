from django.urls import path, re_path

from core.views import PeopleDetailViewSet, PeopleListViewSet, ping

urlpatterns = [
    path('ping/', ping, name='ping'),
    path('people/', PeopleListViewSet.as_view(), name='people-list'),
    path('people/<int:pk>/', PeopleDetailViewSet.as_view(), name='people-detail')
]
