from django.test import TestCase
from rest_framework.reverse import reverse

from core.models import PeopleModel
from core.serializers import PeopleSerializer


class TestPeople(TestCase):
    def setUp(self):
        super().setUp()
        self.user = PeopleModel.objects.create(name='foobar',
                                               email='foobar+test@mail.com',
                                               phone_number='+91' + '9' * 10)

    def test_get_people_list(self):
        response = self.client.get(reverse('people-list'))
        self.assertEqual(response.status_code, 200)
        serializer = PeopleSerializer(instance=self.user)
        self.assertTrue(serializer.data, response.data)

    def test_get_people_detail(self):
        response = self.client.get(reverse('people-detail', args=f'{self.user.pk}'))
        self.assertEqual(response.status_code, 200)
        serializer = PeopleSerializer(instance=self.user)
        self.assertTrue(serializer.data, response.data)

    def test_post_create_people(self):
        response = self.client.post(reverse('people-list'), data={'name': 'foobar2',
                                                                  'email': 'foobar2+test@mail.com',
                                                                  'phone_number': '0123456789'})
        self.assertEqual(response.status_code, 201)

    def test_put_update_people(self):
        new_data = {'name': 'foobar',
                    'email': 'foobar+test@mail.com',
                    'phone_number': '9182634750'}
        response = self.client.put(reverse('people-detail', args=f'{self.user.pk}'),
                                   data=new_data,
                                   content_type='application/json')
        self.user.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.user.phone_number, new_data['phone_number'])

    def test_delete_people(self):
        response = self.client.delete(reverse('people-detail', args=f'{self.user.pk}'))
        self.assertEqual(response.status_code, 204)
        user = PeopleModel.objects.filter(name='foobar')
        self.assertFalse(user.exists())
