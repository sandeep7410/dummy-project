from rest_framework import serializers

from core.models import PeopleModel
from core.validators import validate_name_structure


class PeopleSerializer(serializers.ModelSerializer):
    class Meta:
        model = PeopleModel
        fields = ['name', 'email', 'phone_number']

    def validate_name(self, name):
        if name is None or len(name.strip()) < 1:
            # generate the name after the validations
            return name

        name = name.strip()
        if name.startswith('___'):
            raise serializers.ValidationError('name cannot start with 3 underscores.')

        validate_name_structure(name)
        return name
