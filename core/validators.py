import re

from rest_framework.exceptions import ValidationError


def validate_name_structure(name):
    """
    Validate for the structural pattern of a name.
    """
    name = name.strip()
    RE_NAME_ONLY_UNDESCORES = re.compile('^[_]+$', re.IGNORECASE)
    RE_name = re.compile('^[a-zA-Z0-9_]+$', re.IGNORECASE)

    if len(name) < 3:
        raise ValidationError('Ensure this value has at least 3 characters.')

    if len(name) > 30:
        raise ValidationError('Ensure this value has at most 30 characters.')

    # Just underscores are not allowed.
    if RE_NAME_ONLY_UNDESCORES.match(name):
        raise ValidationError('Only underscores are not valid.')

    # Match Regular Expression to check our enforced format is matched.
    if not RE_name.match(name):
        raise ValidationError('Only letters, numbers and underscores (_) are allowed in name.')
