import logging

from django.http.response import JsonResponse
from django.views.decorators.http import require_GET
from rest_framework import generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from core.models import PeopleModel
from core.serializers import PeopleSerializer
from utils.monitoring import record_exception

logger = logging.getLogger(__name__)


@require_GET
def ping(request):
    return JsonResponse(status=200, data={})


class PeopleListViewSet(generics.ListCreateAPIView):
    queryset = PeopleModel.objects.all()
    serializer_class = PeopleSerializer

    def post(self, request, *args, **kwargs):
        logger.info('[People] creating new people')
        try:
            return super().post(request, *args, **kwargs)
        except ValidationError as e:
            logger.error(f'[People] error in creating new people {e}')
            record_exception()
        return Response(status=status.HTTP_409_CONFLICT)


class PeopleDetailViewSet(generics.RetrieveUpdateDestroyAPIView):
    queryset = PeopleModel.objects.all()
    serializer_class = PeopleSerializer
