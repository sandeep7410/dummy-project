from django.db import models
from django.utils.translation import ugettext_lazy as _

from core.validators import validate_name_structure


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class PeopleModel(BaseModel):
    name = models.CharField(max_length=30, unique=True,
                            verbose_name=_('name'),
                            help_text=_('Required. 3-30 characters. Letters, numbers and _ characters'),
                            validators=[validate_name_structure])
    email = models.EmailField(max_length=72, unique=True, blank=False, null=False)
    phone_number = models.CharField(max_length=15, null=True, blank=True)

    def __str__(self):
        return f'{self.name}'
