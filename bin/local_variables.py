envars = dict(
    DATABASE_URL='postgres://dummy:dummy@127.0.0.1/dummy',

    DEBUG='True',
    DEFAULT_FILE_STORAGE='django.core.files.storage.FileSystemStorage',
    DEVELOPER_EMAIL='creater@developers.com',
    DEVELOPER_NAME='elon musk',
    PATH='`pwd`/bin:$PATH',
    PORT='7890',
    PYTHONPATH='`pwd`:$PYTHONPATH',
    SECRET_KEY='secret',
    ENABLE_DJ_TOOLBAR='True',
    NOSE_WITH_FIXTURE_BUNDLING='True',
    TESTS_SKIP_MIGRATIONS='True',
)
