from sentry_sdk import capture_exception, capture_message, configure_scope


def record_exception(params=None):
    if params is None:
        params = {}

    # Record exception in Sentry along with custom params.
    with configure_scope() as scope:
        for key, value in params.items():
            scope.set_extra(key, value)

    capture_exception()


def record_log(message, params=None):
    if params is None:
        params = {}

    # Record message in Sentry along with custom params.
    with configure_scope() as scope:
        for key, value in params.items():
            scope.set_extra(key, value)

    capture_message(message)


def add_custom_parameter(key, value):

    with configure_scope() as scope:
        scope.set_extra(key, value)


def add_custom_parameters(params):

    for key, value in params.items():
        add_custom_parameter(key, value)
