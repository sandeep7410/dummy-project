import os


def load_local_variables():
    """
    Loads environment variables from bin/local_variables.py
    """
    from bin.local_variables import envars

    for v in envars:
        os.environ.setdefault(v, envars[v])
